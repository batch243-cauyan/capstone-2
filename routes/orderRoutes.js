const express = require('express');
const router = express.Router();
const orderController = require("../controllers/orderControllers");
const auth = require ('../auth.js');








//USER ENDPOINT
//endpoint for 
router.post('/placeorder', auth.verify, orderController.placeOrder);

//USER ENDPOINT
//endpoint for 
router.get('/', auth.verify, orderController.getAllOrders);

router.get('/filterorder', auth.verify, orderController.filterOrders);

router.post('/singleorder', auth.verify, orderController.singleOrder);

router.patch(`/singleorder/approve`, auth.verify, orderController.approveSingleOrder);

router.patch(`/approvecartorders`,auth.verify, orderController.approveOrders)


module.exports = router;