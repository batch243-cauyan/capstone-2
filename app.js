require('dotenv').config();
const express  = require('express');
const app = express();
const mongoose = require('mongoose');
const cors = require('cors');

const productRoutes = require('./routes/productRoutes');
const userRoutes = require('./routes/userRoutes');
const reportRoutes = require('./routes/reportRoutes')
const orderRoutes = require('./routes/orderRoutes')

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use('/products',productRoutes);
app.use('/user', userRoutes);
app.use('/reports', reportRoutes)
app.use('/order', orderRoutes)

const port = process.env.PORT || 4000

//change to myURI for the atlasDB
mongoose.connect(process.env.localURI, {
    useNewUrlParser: true,
    useUnifiedTopology:true
})
.then(()=>{console.log('connected to cloudDB');
app.listen(port, console.log(`Server started on port ${port}`))
})
.catch((error)=>{
    console.log(error);
})



