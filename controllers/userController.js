const User = require('../models/user');
const auth = require('../auth');

const bcrypt = require('bcrypt');



        // check if email format is valid
module.exports.isEmail =  (req, res, next)=>{

                let regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                let emailAddress = req.body.contacts.email;

                if (emailAddress.match(regex)) {
                     next();
                }
                else { 
                    return res.send(`Enter a valid email address.`)
                }
            }
        


module.exports.checkEmailExists =  async (req, res, next)=>{
    // let emailAdd = String(req.body.contacts.email).toLowerCase();
    let emailAdd = req.body.contacts.email;
     
        await User.find( {"contacts.email":emailAdd} ).then(result=>{
            console.log(result);
            let message = "";
            if(result.length > 0){
                message = `The ${emailAdd} is already taken, please use a different email.`
                return res.status(200).json({error:message})
            }  
            else{ next(); }
        }).catch(error => res.send(error))
}            

module.exports.registerUser =  async (req,res) =>{
 
      let newUser = new User ({
        firstName:  req.body.firstName,
        middleName: req.body.middleName,
        lastName:   req.body.lastName,
        contacts: {
            mobileNum: req.body.contacts.mobileNum,
            email:     req.body.contacts.email
        },
        password:   bcrypt.hashSync(req.body.password, 10),   
        address:{
            province:     req.body.address.province,
            municipality: req.body.address.municipality,
            barangay:     req.body.address.barangay,
            houseNum:     req.body.address.houseNum,
            street:       req.body.address.street,
            zipcode:      req.body.address.zipcode
           }
        })

    
            return  newUser.save()
            .then(()=>res.status(201).send(`You have succesfully registered.`))
            .catch(error=>{
                console.log(error);
                res.status(400).send(`Sorry, there was an error during the registration. Please Try again!`)
            })
}





module.exports.getAllUser = async (req,res)=>{
    let userData = auth.decode(req.headers.authorization)

    if(userData.isAdmin){

        await User.find({}).sort({lastName:1})
        .then(result=>
            {return res.status(200).json({users:result})}
            )
        .catch(error=>{
            res.status(204).json({message: `No users found.`, error:error})
        })
    }
    else{
        return res.status(200).json({message:"You do not have admin priveleges."});
    }
}



module.exports.loginUser =  (req,res) =>{
    let emailAdd = req.body.email
    // console.log("This is req.body ",req.body)
    // if(req.body.email !== null) {
        return User.findOne({"contacts.email":emailAdd})
        .then((user => {
            
            if (!user){
                return res.send(`No registered account under ${emailAdd}. Please Register first.`) 
            }
            else{
       
                const isPasswordCorrect = bcrypt.compareSync(req.body.password, user.password);

                if(isPasswordCorrect){
                    return res.send({accessToken: auth.createAccessToken(user)})
                    // return res.send(`Logged in successfully!`)
                }

                    return res.send(`Incorrect password, please try again!`)
            }

        }))
    // }
}


module.exports.updateRole = async (req,res)=>{

    const token = req.headers.authorization;
    const userData = auth.decode(token);

    let idToBeUdated = req.params.userId

    if (userData.isAdmin){

        await User.findById(idToBeUdated)
           .then(result=>{
                let update = !result.isAdmin;
            
            return User.findByIdAndUpdate(idToBeUdated, {isAdmin : update}, {new:true})
                    .then(result => {
                        result.password = "Confidential";
                        res.status(200).json({user:result})
                    })
          })
          .catch(err => res.json({error:err}))

    }
    else{
        return res.send("You don't have access on this page!")
    }

}

module.exports.profileDetails = async (req,res) =>{
    // user will be object that contains the id and email of the user that is currently logged in.
    const userData = auth.decode(req.headers.authorization);
    console.log(userData);
    await User.findById(userData.id).populate('userTransactions.orderId')
    .then(result=>{
        result.password = "Confidential";
        return res.send(result)
    }).catch(err => {
        return res.send(err);
    })
}


module.exports.viewCart = async (req,res)=>{

    const userData = auth.decode(req.headers.authorization);

    await User.findById(userData.id, {firstName:1, lastName:1, shoppingCart:1, totalamt: 1}).populate
    //second argument limits the fields returned by populate
    ('shoppingCart.productId', ['prodName' ,'price','discountedPrice'] ).then(result=>{
        
        if(result.shoppingCart.length < 1){
            return res.status(200).json({message:"Cart is empty"});
        }


        return res.status(200).json({
            count : result.shoppingCart.length,
            "Items in cart":result.shoppingCart});
    }).catch(err => {
        return res.send(err);
    })
}



